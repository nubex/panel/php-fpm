[![pipeline](https://gitlab.com/nubex/panel/php-fpm/badges/master/pipeline.svg)](https://gitlab.com/nubex/panel/php-fpm/pipelines)

### Назначение
Этот образ используется в проекте [Панель Нубекса](https://gitlab.com/nubex/nubex-panel).

Образ предназначен для использования:

-  как образ для запуска проекта на [production](https://gitlab.com/nubex/panel/panel-prod).
-  для построения [dev образа](https://gitlab.com/nubex/panel/php-fpm-dev).



